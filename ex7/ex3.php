<?php

require_once 'vendor/tpl.php';
require_once 'Request.php';

$request = new Request($_REQUEST);

// print $request; // display input parameters (for debugging)

$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'ctf_form';

if ($cmd === 'ctf_form' || $cmd === 'ftc_form') {

    if ($cmd === 'ctf_form')
        $cmd = 'ctf_calculate';
    else 
        $cmd = 'ftc_calculate';

    $data = [
        'template' => 'ex3_form.html',
        'cmd' => $cmd,
    ];

    print renderTemplate('tpl/ex3_main.html', $data);

} else if ($cmd === 'ctf_calculate' || $cmd === 'ftc_calculate') {
    $input = $request->param('temperature');
    $errors = [];
    $template = 'ex3_result.html';
    $message = '';
    
    if (is_numeric($input)) {
        $result = celsiusToFahrenheit($input);
        $message = "$input degrees in Celsius is $result degrees in Fahrenheit";

        if ($cmd === 'ctf_calculate') {
            $result = celsiusToFahrenheit($input);
            $message = "$input degrees in Celsius is $result degrees in Fahrenheit";
        } else {
            $result = fahrenheitToCelsius($input);
            $message = "$input degrees in Fahrenheit is $result degrees in Celsius";
        }
    } else {
        $template = 'ex3_form.html';
        array_push($errors, "Input must be a number!");
    }

    $data = [
        'input' => $input,
        'template' => $template,
        'message' => $message,
        'errors' => $errors,
        'cmd' => $cmd
    ];

    print renderTemplate('tpl/ex3_main.html', $data);

} else {
    throw new Error('programming error');
}

function celsiusToFahrenheit($temp) : float {
    return round($temp * 9 / 5 + 32, 2);
}

function fahrenheitToCelsius($temp) : float {
    return round(($temp - 32) / (9 / 5), 2);
}

