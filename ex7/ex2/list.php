<?php
require_once '../vendor/tpl.php';
require_once('./Book.php');
require_once('./Author.php');

$book1 = new Book("Head First HTML and CSS", 5, false);
$book1->addAuthor(new Author("Elisabeth", "Robson"));
$book1->addAuthor(new Author("Eric", "Freeman"));

$book2 = new Book("Learning Web Design", 4, true);
$book2->addAuthor(new Author("Jennifer", "Robbins"));

$book3 = new Book("Head First Learn to Code", 3, false);
$book3->addAuthor(new Author("Eric", "Freeman"));

$books = [$book1, $book2, $book3];

$data = [
    'books' => $books
];

print renderTemplate('tpl/list.html', $data);
?>
