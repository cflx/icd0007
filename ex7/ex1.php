<?php

require_once 'OrderLine.php';
require_once 'OrderLineDao.php';

$path = 'data/order.txt';

$dao = new OrderLineDao($path);

// print list of order line objects
foreach ($dao->getOrderLines() as $orderLine) {
    printf('name: %s, price: %s; in stock: %s' . PHP_EOL,
        $orderLine->productName,
        $orderLine->price,
        $orderLine->inStock ? 'true' : 'false');
}

