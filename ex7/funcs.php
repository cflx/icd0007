<?php

function celsiusToFahrenheit($temp) : float {
    return round($temp * 9 / 5 + 32, 2);
}

function fahrenheitToCelsius($temp) : float {
    return round(($temp - 32) / (9 / 5), 2);
}
