<?php 
class Book {
    public int $id;
    public string $title;
    public int $grade;
    public int $is_read;
    public array $authors;

    public function __construct(int $id, string $title, int $grade, int $is_read, array $authors) {
        $this->id = $id;
        $this->title = $title;
        $this->grade = $grade;
        $this->is_read = $is_read;
        $this->authors = $authors;
    }

    public function __toString()
    {
        return $this->firstname . ' ' . $this->lastname;       
    }
}