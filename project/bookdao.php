<?php
require_once('dbcontext.php');
require_once('book.php');

class BookDao {

    private PDO $conn;

    public function __construct() {
        $this->conn = getConnection();
    }

    public function validateInputBook(Book $book) 
    {
        $e = [];
        $len = strlen($book->title);
    
        if($len > 23 || $len < 3)
            array_push($e, "Pealkiri peab olema pikem kui 3 ja lühem kui 23 tähte!");
    
        return $e;
    }
    
    public function addBook(Book $new)
    {
        $stmt = $this->conn->prepare('INSERT INTO book(title, grade, is_read) VALUE (:title, :grade, :is_read)');
        $stmt->execute(array(':title' => $new->title, ':grade' => $new->grade, ':is_read' => $new->is_read));
    
        $book_id = $this->conn->lastInsertId();
    
        $this->addAuthorsToBook($book_id, $new->authors);
    }
    
    public function addAuthorsToBook(int $book_id, array $author_ids) {
        if ($book_id == -1) return;
    
        foreach($author_ids as $author_id) {
            if ($author_id == -1) continue;
            $stmt = $this->conn->prepare('INSERT INTO is_author(book_id, author_id) VALUE (:book_id, :author_id)');
            $stmt->execute(array(':book_id' => $book_id, ':author_id' => $author_id));
        }        
    }
    
    public function getBook(int $id)
    {
        $stmt = $this->conn->prepare(
                'SELECT b.id as book_id, b.title, b.grade, b.is_read, ia.author_id, a.firstname, a.lastname FROM book AS b
                    LEFT JOIN is_author ia ON (b.id = ia.book_id)
                    LEFT JOIN author a ON (a.id = ia.author_id)
                    WHERE b.id = :id'
                );
        $stmt->execute(array(':id' => $id));
        $result = $stmt->fetchAll();
    
        if ($result == []) return;
    
        $authors = [];
    
        foreach ($result as $line) {
            if (!is_null($line['author_id']))
                array_push($authors, new Author($line['author_id'], $line['firstname'], $line['lastname'], 0));
        }
    
        return new Book($result[0]['book_id'],
                        $result[0]['title'],
                        $result[0]['grade'],
                        $result[0]['is_read'],
                        $authors);
    }
    
    public function getAllBooks()
    {
        $stmt = $this->conn->prepare(
                'SELECT b.id as book_id, b.title, b.grade, b.is_read, ia.author_id, a.firstname, a.lastname FROM book AS b
                    LEFT JOIN is_author ia ON (b.id = ia.book_id)
                    LEFT JOIN author a ON (a.id = ia.author_id)'
                );
        $stmt->execute();
        $result = $stmt->fetchAll();
    
        $books = [];
    
        if ($result == []) return $books;
    
        $book = new Book($result[0]['book_id'], $result[0]['title'], $result[0]['grade'], $result[0]['is_read'], []);
        $authors = [];
        foreach ($result as $line) {
            if ($line['book_id'] != $book->id) {
                $book->authors = array_reverse($authors);
                array_push($books, $book);
                $book = new Book($line['book_id'], $line['title'], $line['grade'], $line['is_read'], array());
                $authors = [];
            }
            if(isset($line['author_id']))
                array_push($authors, new Author($line['author_id'], $line['firstname'], $line['lastname'], 0));
        }
    
        $book->authors = $authors;
        array_push($books, $book);
    
        return $books;
    }
    
    public function deleteBook(int $id)
    {
        $stmt = $this->conn->prepare('DELETE FROM book WHERE id = :id');
        $stmt->execute(array('id' => $id));
    }
    
    public function editBook(int $id, Book $new) 
    {
        $stmt = $this->conn->prepare('UPDATE book SET title = :title, grade = :grade, is_read = :is_read
                                    WHERE id = :id');
        $stmt->execute(array(':title' => $new->title, ':grade' => $new->grade, ':is_read' => $new->is_read, ':id' => $id));
    
        $stmt = $this->conn->prepare('DELETE FROM is_author WHERE book_id = :book_id');
        $stmt->execute(array(':book_id' => $id));
    
        $this->addAuthorsToBook($id, $new->authors);
    }
}