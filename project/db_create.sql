CREATE TABLE book (
 id INTEGER PRIMARY KEY AUTO_INCREMENT,
 title VARCHAR(255) NOT NULL,
 grade INTEGER,
 is_read bool
);

CREATE TABLE is_author (
 book_id INTEGER NOT NULL,
 author_id INTEGER NOT NULL,
 
 INDEX book_ind (book_id),
 FOREIGN KEY (book_id)
   REFERENCES book(id)
   ON DELETE CASCADE,
   
 INDEX author_ind (author_id),
 FOREIGN KEY (author_id)
   REFERENCES author(id)
   ON DELETE CASCADE
);


CREATE TABLE author (
  id INTEGER PRIMARY KEY AUTO_INCREMENT,
  firstname VARCHAR(128) NOT NULL,
  lastname VARCHAR(128) NOT NULL,
  grade INTEGER
);

/* 
DROP TABLE book;
DROP TABLE is_author;
DROP TABLE author;
*/
