<?php
class Author {
    public int $id;
    public string $firstname;
    public string $lastname;
    public int $grade;

    public function __construct(int $id, string $firstname, string $lastname, int $grade) {
        $this->id = $id;
        $this->firstname = $firstname;
        $this->lastname = $lastname;
        $this->grade = $grade;
    }

    public function __toString()
    {
        return $this->firstname . ' ' . $this->lastname;
    }

}