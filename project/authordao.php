<?php
require_once('dbcontext.php');
require_once('author.php');

class AuthorDao {
    private PDO $conn;

    public function __construct() {
        $this->conn = getConnection();
    }
    
    public function validateInputAuthor(Author $author) 
    {
        $errors = [];
    
        $name_len = strlen($author->firstname);
        if($name_len > 22 || $name_len < 1)
            array_push($errors, "Eesimi peab olema pikem kui 1 ja lühem kui 22 tähte!");
            
        $name_len = strlen($author->lastname);
        if($name_len > 22 || $name_len < 2)
            array_push($errors, "Perekonnanimi peab olema pikem kui 1 ja lühem kui 22 tähte!");
    
        return $errors;
    }
    
    public function addAuthor(Author $new)
    {
        $stmt = $this->conn->prepare('INSERT INTO author(firstname, lastname, grade) VALUE (:firstname, :lastname, :grade)');
        $stmt->execute(array(':firstname' => $new->firstname, ':lastname' => $new->lastname, ':grade' => $new->grade));
    }
    
    public function getAuthor(int $id)
    {
        $stmt = $this->conn->prepare('SELECT * FROM author WHERE id = :id');
        $stmt->execute(array(':id' => $id));
        $result = $stmt->fetchAll()[0];
    
        return new Author($result['id'], $result['firstname'], $result['lastname'], $result['grade']);
    }
    
    public function getAllAuthors()
    {
        $stmt = $this->conn->prepare('SELECT * FROM author');
        $stmt->execute();
    
        $authors = [];
        foreach ($stmt->fetchall() as $result)
            array_push($authors, new Author($result['id'], $result['firstname'], $result['lastname'], $result['grade']));
    
        return $authors;
    }
    
    public function deleteAuthor(int $id)
    {
        $stmt = $this->conn->prepare('DELETE FROM author WHERE id = :id');
        $stmt->execute(array('id' => $id));
    }
    
    public function editAuthor(int $id, Author $new) 
    {
        $stmt = $this->conn->prepare('UPDATE author SET firstname = :firstname, lastname = :lastname, grade = :grade
                                    WHERE id = :id');
        $stmt->execute(array(':firstname' => $new->firstname, ':lastname' => $new->lastname,
                             ':grade' => $new->grade, 'id' => $id));
    }
}