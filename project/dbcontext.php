<?php
const HOST = '';
const USERNAME = '';
const PASSWORD = '';

function getConnection() : PDO {
    $address = sprintf('mysql:host=%s;port=3306;dbname=%s',
                        HOST, USERNAME);

    $db_connection = new PDO($address, USERNAME, PASSWORD, array(
        PDO::ATTR_PERSISTENT => true));

    return $db_connection;
}
