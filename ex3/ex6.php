<?php

include_once __DIR__ . '/Post.php';

const DATA_FILE = __DIR__ . '/data/posts.txt';

// printPosts(getAllPosts());

savePost(new Post('Html', "some text about html"));

// printPosts(getAllPosts());

function getAllPosts() : array {

    $posts = file(DATA_FILE);

    for ($i = 0; $i < count($posts); $i++) {
        $post = explode(';', $posts[$i]);
        $posts[$i] = new Post($post[0], urldecode(trim($post[1])));
    }

    return $posts;
}

function savePost(Post $post) : void {
    $post_string = $post->title . ';' . urlencode($post->text) . "\r\n";

    $fp = fopen(DATA_FILE, 'a');//opens file in append mode  
    fwrite($fp, $post_string);  
    fclose($fp);  
}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}

?>