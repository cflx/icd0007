<?php

for ($i = 1; $i <= 15; $i++) {
    $output = "";

    if ($i % 5 == 0)
        $output = $output . "Fizz";
    if ($i % 3 == 0)
        $output = $output . "Buzz";
    if ($output == "")
        $output = $i;

    print ($output . "\r\n");
}

?>