<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Celsius to Fahrenheit</title>
</head>
<body>

    <nav>
        <a href="c2f.html">Celsius to Fahrenheit</a> |
        <a href="f2c.html">Fahrenheit to Celsius</a>
    </nav>

    <main>

        <h3>Celsius to Fahrenheit</h3>
        
        <em>
            <?php

            $data = $_GET["temperature"];

            if (!$data)
                print("Insert temperature");
            else if (!is_numeric($data))
                print ("Temperature must be an integer");
            else {
                $temp = intval($data);
                printf("%d degrees in Celsius is %d decrees in Fahrenheit", $temp, round($temp * 9/5 + 32));
            }

            ?>
        </em>
        
    </main>

</body>
</html>
