<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Fahrenheit to Celsius</title>
</head>
<body>

    <nav>
        <a href="c2f.html">Celsius to Fahrenheit</a> |
        <a href="f2c.html">Fahrenheit to Celsius</a>
    </nav>

    <main>

        <h3>Fahrenheit to Celsius</h3>

        <em>
            <?php

            $data = $_GET["temperature"];

            if (!$data)
                print("Insert temperature");
            else if (!is_numeric($data))
                print ("Temperature must be an integer");
            else {
                $temp = intval($data);
                printf("%d degrees in Fahrenheit is %d decrees in Celsius", $temp, round(($temp - 32) / (9/5)));
            }

            ?>
        </em>

    </main>

</body>
</html>