<?php

$numbers = [1, 2, '3', 6, 2, 3, 2, 3];

function isInList($list, $elementToBeFound) : bool {
    foreach ($list as $element) {
        if ($element == $elementToBeFound)
            return true;
    }

    return false;
}

?>