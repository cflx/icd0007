<?php

$text = $_GET['text'] ?? '';

if (!empty($text)) {
    $text_formatted = urlencode("Data was: " . $text);
    header("Location: receiver.php?text=" . $text_formatted);
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>

<form>
    <label for="ta">Message:</label>
    <br>
    <textarea id="ta" name="text"></textarea>
    <br>
    <button name="sendButton" type="submit" >Send</button>
</form>

</body>
</html>
