<?php

$numbers = [1, 2, 5, 6, 2, 11, 2, 7];

function getOddNumbers($list) : array {
    $odd_numbers = [];

    foreach($list as $element) {
        if ($element % 2 == 1)
            array_push($odd_numbers, $element);
    }

    return $odd_numbers;
}

?>