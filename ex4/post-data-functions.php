<?php

include_once __DIR__ . '/Post.php';

const ID_DATA_FILE = __DIR__ . '/data/id.txt';
const DATA_FILE = __DIR__ . '/data/posts.txt';

$post = new Post('Title 1', 'Text 1');

savePost($post);

function savePost(Post $post) : int {
    $id = -1;
    if ($post->id == "") {
        $id = getNewID();
        $post->id = $id;

    $line = urlencode($post->id) . ';' .urlencode($post->title) . ';' . urlencode($post->text) . PHP_EOL;

    file_put_contents(DATA_FILE, $line, FILE_APPEND);
    } else {
        $id = $post->id;
        editPostById($post->id, $post->title, $post->text);
    }
    return $id;
}

function editPostById(string $id, string $newtitle, string $newtext) {
    $posts = getAllPosts();
    $data = "";

    foreach ($posts as $post) {
        if ($post->id == $id) {
            $data = $data . urlencode($post->id) . ";" 
                          . urlencode($newtitle) . ";" 
                          . urlencode($newtext) . PHP_EOL;
        } else {
            $data = $data . urlencode($post->id) . ";" 
                          . urlencode($post->title) . ";" 
                          . urlencode($post->text) . PHP_EOL;
        }
    }

    file_put_contents(DATA_FILE, $data);
}

function deletePostById(string $id) : void {
    $posts = getAllPosts();
    $data = "";

    foreach ($posts as $post) {
        if (!($post->id == $id)) {
            $data = $data . urlencode($post->id) . ";" 
                          . urlencode($post->title) . ";" 
                          . urlencode($post->text) . PHP_EOL;
        }
    }

    file_put_contents(DATA_FILE, $data);
}

function getAllPosts() : array {

    $lines = file(DATA_FILE);

    $result = [];
    foreach ($lines as $line) {
        [$id, $title, $text] = explode(';', trim($line));

        $post = new Post(urldecode($title), urldecode($text));
        $post->id = $id;

        $result[] = $post;
    }

    return $result;
}

function printPosts(array $posts) {
    foreach ($posts as $post) {
        print $post . PHP_EOL;
    }
}

function getNewID() : string {
    $file = file(ID_DATA_FILE);
    
    file_put_contents(ID_DATA_FILE, intval($file[0]) + 1);

    return $file[0];
}