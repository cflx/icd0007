<?php

$data = $_POST['data'] ?? '';
str_replace("\n", "</br>", $data);

$url = 'index.php?message=' . urlencode("Data saved!" ."\n" . "data: " . $data);

header('Location: ' . $url);
