<?php
require_once 'project/request.php';
require_once 'project/authordao.php';
require_once 'project/bookdao.php';
require_once 'project/vendor/tpl.php';

$request = new Request($_REQUEST);

$cmd = $request->param('cmd')
    ? $request->param('cmd')
    : 'book-list';

if ($cmd === 'book-list') 
{ 
    $db = new BookDao();
    $books = $db->getAllBooks();

    $data = [
      'template' => 'book_list.html',
      'page_title' => 'Book List',
      'body_id' => 'book-list-page',
      'message' => $request->param('msg') ?? '',
      'books' => $books   
    ];

    print renderTemplate('project/views/main.html', $data);

} 
else if ($cmd === 'author-list') 
{
    $db = new AuthorDao();
    $authors = $db->getAllAuthors();

    $data = [
      'template' => 'author_list.html',
      'page_title' => "Author List",
      'body_id' => 'author-list-page',
      'message' => $request->param('msg') ?? '',
      'authors' => $authors   
    ];

    print renderTemplate('project/views/main.html', $data);
} 
else if ($cmd === 'author-form' || $cmd === 'author-edit') 
{
    $errors = [];
    $id = $request->param("id") ?? -1;
    $firstname = $request->param("firstName") ?? "";
    $lastname = $request->param("lastName") ?? "";
    $grade = $request->param("grade") ?? 0;

    if ($_SERVER["REQUEST_METHOD"] == "POST") 
    {
        $db = new AuthorDao();
        $action = "";

        if (!is_null($request->param("submitButton")))
            $action = "submit";
        if (!is_null($request->param("deleteButton")))
            $action = "delete";
        
        if ($action == "submit")
        {
            $author = new Author(urldecode($id), $firstname, $lastname, intval($grade));
            $errors = $db->validateInputAuthor($author);

            if(sizeof($errors) == 0) 
            {
                if($cmd == "author-edit")
                {  
                    $db->editAuthor(urldecode($id), $author);
                    header("Location: ?cmd=author-list&msg=Muudetud!");
                    return;
                } 
                else 
                {
                    $db->addAuthor($author);
                    header("Location: ?cmd=author-list&msg=Salvestatud!");
                    return;
                }
            }
        }
        if ($action == "delete")
        {
            $db->deleteAuthor($id);
            header("Location: ?cmd=author-list&msg=Kustutatud!");
            return;
        }
    }

    if ($id != -1) 
    {
        $db = new AuthorDao();
        $author = $db->getAuthor(intval($id));
        $firstname = $author->firstname;
        $lastname = $author->lastname;
        $grade = $author->grade;
    }

    $data = [
        'template' => 'author_form.html',
        'page_title' => 'Author Form',
        'body_id' => 'author-form-page',
        'cmd' => $cmd,
        'errors' => $errors,
        'id' => $id,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'grade' => $grade
    ];

    print renderTemplate('project/views/main.html', $data);
} 
else if ($cmd === 'book-form' || $cmd === 'book-edit') 
{
    $authordb = new AuthorDao();

    $errors = [];
    $id = $request->param('id') ?? -1;
    $title = $request->param('title') ?? "";
    $authors = $authordb->getAllAuthors();
    $book_authors = [is_numeric($request->param('author1')) ? $request->param('author1') ?? -1 : -1,
                     is_numeric($request->param('author2')) ? $request->param('author2') ?? -1 : -1];
    $grade = $request->param('grade') ?? 0;
    $read = is_null($request->param('isRead')) ? 0 : 1;

    if($_SERVER["REQUEST_METHOD"] == "POST")
    {
        $db = new BookDao();

        if (!is_null($request->param("submitButton"))) 
            $action = "submit";
        if (!is_null($request->param("deleteButton"))) 
            $action = "delete";

        if ($action == "submit")
        {
            $book = new Book(urldecode($id), $title, intval($grade), intval($read),
                             [intval($book_authors[0]), intval($book_authors[1])]);
            $errors = $db->validateInputBook($book);
            if(sizeof($errors) == 0) 
            {
                if ($cmd == "book-edit")
                {
                    $db->editBook(urldecode($id), $book);
                    header("Location: ?cmd=book-list&msg=Muudetud!");
                    return;
                } 
                else
                {
                    $db->addBook($book);
                    header("Location: ?cmd=book-list&msg=Salvestatud!");
                    return;
                }
            }
        }
        if ($action == "delete")
        {
            $db->deleteBook($id);
            header("Location: ?cmd=book-list&msg=Kustutatud!");
            return;
        }
    } 

    if ($id != -1) 
    {
        $db = new BookDao();
        $book = $db->getBook(intval($id));
        $id = $book->id;
        $title = $book->title;
        $grade = $book->grade;
        $read = $book->is_read;
        $book_authors = [$book->authors[0]->id ?? -1,
                         $book->authors[1]->id ?? -1];
    }

    $data = [
        'template' => 'book_form.html',
        'page_title' => 'Book Form',
        'body_id' => 'book-form-page',
        'cmd' => $cmd,
        'errors' => $errors,
        'id' => $id,
        'title' => $title,
        'read' => $read,
        'grade' => $grade,
        'book_authors' => $book_authors,
        'authors' => $authors
    ];

    print renderTemplate('project/views/main.html', $data);
}

?>
