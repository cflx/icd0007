<?php

require_once 'connection.php';

function addAndPrintNumbers() {
    $conn = getConnection();

    $stmt = $conn->prepare('TRUNCATE TABLE number');
    $stmt->execute();

    for($i = 0; $i <= 100; $i++) {
        $value = rand(1, 100);
        $stmt = $conn->prepare('INSERT INTO number(number) VALUE (:number)');
        $stmt->bindValue(':number', $value);
        $stmt->execute();
    }

    $min_value = 80;

    $stmt = $conn->prepare('SELECT * FROM number WHERE number.number > :min');
    $stmt->bindValue(":min", $min_value);
    $stmt->execute();

    foreach($stmt as $line) {
        printf("%d \n", $line['number']);
    }
}

// addAndPrintNumbers();

function addContactWithNumbers(string $name, array $numbers) {
    $conn = getConnection();

    $stmt = $conn->prepare('INSERT INTO contact (name) VALUE (:firstname)');
    $stmt->bindValue(':firstname', $name);
    $stmt->execute();

    $contact_id = $conn->lastInsertId();

    foreach ($numbers as $number) {
        $stmt = $conn->prepare('INSERT INTO phone (contact_id, number) VALUE (:contact_id, :number)');
        $stmt->bindValue(':contact_id', $contact_id);
        $stmt->bindValue(':number', $number);
      
        $stmt->execute();
    }
}

// addContactWithNumbers("Meelis", [123, 223, 323]);
