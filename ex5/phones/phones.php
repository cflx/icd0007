<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/Contact.php';

function getContacts() : array {
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, name FROM contact');
    $stmt->execute();
    $contacts = $stmt->fetchAll();

    $stmt = $conn->prepare('SELECT contact_id, number FROM phone');
    $stmt->execute();
    $numbers = $stmt->fetchAll();
    
    $all_contacts = [];

    foreach ($contacts as $contact) {
        $new = new Contact($contact["id"], $contact["name"]);

        foreach ($numbers as $number)
            if ($number["contact_id"] == $contact["id"])
                $new->addPhone($number["number"]);
                
        array_push($all_contacts, $new);
    }

    return $all_contacts;
}

// var_dump(getContacts());