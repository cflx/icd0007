<?php

require_once __DIR__ . '/../connection.php';
require_once __DIR__ . '/MenuItem.php';

function getMenu() : array {
    $conn = getConnection();

    $stmt = $conn->prepare('SELECT id, parent_id, name 
                            FROM menu_item ORDER BY id');
    $stmt->execute();

    $all_menuitems = [];
    $root_menuitems = [];

    foreach ($stmt as $row) {
        $new = new MenuItem($row["id"], $row["name"]);

        array_push($all_menuitems, $new);
        if ($row["parent_id"] != null) {
            foreach ($all_menuitems as $menuitem)
                if ($menuitem->id == $row["parent_id"])
                    $menuitem->addSubItem($new);
        } else {
            array_push($root_menuitems, $new);

        }
    }

    return $root_menuitems;
}

// var_dump(getMenu());











function printMenu($items, $level = 0) : void {
    $padding = str_repeat(' ', $level * 3);
    foreach ($items as $item) {
        printf("%s%s\n", $padding, $item->name);
        if ($item->subItems) {
            printMenu($item->subItems, $level + 1);
        }
    }
}
