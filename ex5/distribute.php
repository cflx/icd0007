<?php

$sets = distributeToSets([1, 2, 1]);

// var_dump($sets);

function distributeToSets(array $input) : array {
    $result = [];

    foreach ($input as $number) {
        if (key_exists($number, $result)) {
            array_push($result[$number], $number);
        } else {
            $result[$number] = [$number];
        }
    }

    return $result;
}
